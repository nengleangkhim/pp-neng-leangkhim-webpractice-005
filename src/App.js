import './App.css';
import NavMenu from './components/NavMenu';
import React, { Component } from 'react'
import Content from './components/Content';
import TableData from './components/TableData';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          title: "Pizza",
          amount: 0,
          img: 'image/pizza.png',
          price: 15,
          total: 0,
        },
        {
          id: 2,
          title: "Chicken",
          amount: 0,
          img: "image/chicken.jpeg",
          price: 5,
          total: 0,
        },
        {
          id: 3,
          title: "Burgur",
          amount: 0,
          img: "image/istockphoto-1188412964-612x612.jpg",
          price: 6,
          total: 0,
        },
        {
          id: 4,
          title: "Coca Cola",
          amount: 0,
          img: "image/coca.jpg",
          price: 2,
          total: 0,
        }
      ]
    }
    this.onAdd = this.onAdd.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }
 
  onAdd = (index) => {
    let temp = this.state.data
    temp[index].amount++
    temp[index].total = temp[index].amount * temp[index].price
    this.setState({
      data: temp
    })

  }
  onDelete = (index) => {
    let temp = this.state.data
    temp[index].amount--
    temp[index].total = temp[index].amount * temp[index].price
    this.setState({
      data: temp
    })
  }

  onClear = () => {
   
    let data = [...this.state.data]
    data.map(items=>{
        items.amount = 0
        items.total = 0
    })

    this.setState({
      data
    })

}
  render() {
    return (
      <div>
        <NavMenu />
        <Content
          onAdd={this.onAdd}
          onDelete={this.onDelete}
          items={this.state.data} />
          <TableData items={this.state.data} onClear={this.onClear}/>
        
      </div>
    )
  }
}

