import React from 'react'
import { Container, Table , Button} from "react-bootstrap";

export default function TableData(data, onClear) {

    let temp = data.items.filter(items=>{
        return items.amount > 0
    })

    return (
        <Container>    
            <Button onClick={data.onClear} variant="warning">Clear all</Button>
            <h2>{temp.length} data</h2>
            <Table striped bordered hover>
                <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Amount</th>
                    <th>Price</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    {temp.map((items, index) => (
                    <tr>
                        <td>{index+1}</td>
                        <td>{items.title}</td>
                        <td>{items.amount}</td>
                        <td>{items.price}</td>
                        <td>{items.total}</td>
                    </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    );
}


